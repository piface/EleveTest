<?php

class MarksController extends Controller
{
    public $helpers = ['Html', 'Form', 'Flash'];

    public function add($studentId)
    {
        if ($this->request->is('post')) {
            $this->Mark->create(['student_id' => $studentId, 'created_at' => date('Y-m-d h:i:s')]);
            if ($this->Mark->save($this->request->data)) {
                $this->Flash->success(__('La note à été ajouté avec succès.'));
                return $this->redirect(['controller' => 'students', 'action' => 'view', $studentId]);
            }
            $this->Flash->error(__("Impossible d'ajouter la note"));
        }
        $this->set('subjects', $this->Mark->Subject->find('list'));
        $this->set('studentId', $studentId);
    }
}
