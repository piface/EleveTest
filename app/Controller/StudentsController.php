<?php


class StudentsController extends Controller
{
    public $helpers = ['Html', 'Form', 'Flash'];

    public function index()
    {
        $this->set('students', $this->Student->find('all'));
    }

    /**
     * @param null $id
     */
    public function view($id = null)
    {
        $this->Student->recursive = 2;
        $student = $this->Student->findById($id);

        if (empty($student)) {
            throw new NotFoundException(__('Invalid post'));
        }

        $this->set('student', $student);
    }

    /**
     * @return CakeResponse|null
     */
    public function add()
    {
        if ($this->request->is('post')) {
            $this->Student->create();
            if ($this->Student->save($this->request->data)) {
                $this->Flash->success(__('Fiche élève créée avec succès.'));
                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__("Impossible d'ajouter l'élève"));
        }
    }

    /**
     * @param null $id
     * @return CakeResponse|null
     */
    public function edit($id = null)
    {
        $student = $this->Student->findById($id);
        if (empty($student)) {
            throw new NotFoundException(__('Invalid post'));
        }

        if ($this->request->is(['post', 'put'])) {
            $this->Student->id = $id;
            if ($this->Student->save($this->request->data)) {
                $this->Flash->success(__('La fiche élève à été mise à jour'));
                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('Unable to update your post.'));
        }

        if (!$this->request->data) {
            $this->request->data = $student;
        }

        $this->set('student', $student);
    }

    /**
     * @param null $id
     */
    public function delete($id = null)
    {
        if ($this->request->is('get')) {
            throw new MethodNotAllowedException();
        }

        $student = $this->Student->findById($id);

        if ($this->Student->delete($id)) {
            $this->Flash->success(
                __("La fiche élève de l'élève " .
                    $student['Student']['firstname'] . ' ' . $student['Student']['lastname'] .
                    " a été supprimée.")
            );
        } else {
            $this->Flash->success(
                __("La fiche élève n'a pas pu être supprimée")
            );
        }
        $this->redirect(['action' => 'index']);
    }
}
