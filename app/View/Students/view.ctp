<h1>
    Liste des notes
    de <?php echo h($student['Student']['firstname'] . ' ' . h($student['Student']['lastname'])); ?>
</h1>

<?php echo $this->Html->link('Ajouter une note',
    ['controller' => 'marks', 'action' => 'add', 'id' => $student['Student']['id']]);
?>

<?php if (empty($student['Mark'])) { ?>
    <p><i>Aucune note pour cet élève</i></p>
<?php } else { ?>
    <table>
        <tr>
            <th>Matière</th>
            <th>Note</th>
        </tr>

        <?php foreach ($student['Mark'] as $mark): ?>
            <tr>
                <td><?php echo $mark['Subject']['name'] ?></td>
                <td><?php echo $mark['value'] ?>/20</td>
            </tr>
        <?php endforeach; ?>
    </table>

<?php }
echo $this->Html->link('Retour',
    ['controller' => 'students', 'action' => 'index']
);
?>

