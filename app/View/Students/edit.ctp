<h1>Edition de l'élève
    <?php echo h($student['Student']['firstname']) . ' ' . h($student['Student']['lastname']); ?>
</h1>
<?php
echo $this->Form->create('Student');
echo $this->Form->input('firstname');
echo $this->Form->input('lastname');
echo $this->Form->input('birthday');
echo $this->Form->end('Enregistrer les modifications');

echo $this->Html->link('Retour',
    ['controller' => 'students', 'action' => 'index']
);
?>
