<p>
    <?php echo $this->Html->link('Ajouter un élève',
        ['controller' => 'students', 'action' => 'add']
    ) ?>
</p>

<h1>Liste des élèves</h1>


<?php if (empty($students)) { ?>
    <p><i>Aucun élève</i></p>
<?php } else { ?>
    <table>
        <tr>
            <th>Id</th>
            <th>Prénom</th>
            <th>Nom</th>
            <th>Date de naissance</th>
            <th>Actions</th>
        </tr>

        <!-- Here is where we loop through our $posts array, printing out post info -->

        <?php foreach ($students as $student): ?>
            <tr>
                <td><?php echo $student['Student']['id']; ?></td>
                <td><?php echo $student['Student']['firstname']; ?></td>
                <td><?php echo $student['Student']['lastname']; ?></td>
                <td><?php echo date('d/m/Y', strtotime($student['Student']['birthday'])) ?></td>
                <td>
                    <?php echo $this->Html->link('Voir',
                        ['controller' => 'students', 'action' => 'view', $student['Student']['id']]);
                    ?>
                    <?php echo $this->Html->link('Modifier',
                        ['controller' => 'students', 'action' => 'edit', $student['Student']['id']]);
                    ?>
                    <?php echo $this->Form->postLink('Supprimer',
                        ['action' => 'delete', $student['Student']['id']],
                        ['confirm' => 'Voulez vous vraiment supprimer cette ficher ?']);
                    ?>
                </td>
            </tr>
        <?php endforeach; ?>
        <?php unset($student); ?>
    </table>
<?php } ?>
