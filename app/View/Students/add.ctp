<h1>Ajouter un élève</h1>
<?php
echo $this->Form->create('Student');
echo $this->Form->input('firstname', ['label' => 'Prénom']);
echo $this->Form->input('lastname', ['label' => 'Nom']);
echo $this->Form->input('birthday', [
    'label' => 'Date de naissance',
    'dateFormat' => 'DMY',
    'minYear' => date('Y') - 70,
    'maxYear' => date('Y') - 1,
]);

echo $this->Form->end('Sauvegarder le post');
echo $this->Html->link('Retour',
    ['controller' => 'students', 'action' => 'index']
);
?>
