<h1>Ajout d'une note</h1>
<?php
echo $this->Form->create('Mark');
echo $this->Form->input('subject_id', ['label' => 'Matière']);
echo $this->Form->input('value', ['label' => 'Note']);
echo $this->Form->end('Valider la note');

echo $this->Html->link('Retour',
    ['controller' => 'students', 'action' => 'view', $studentId]
);
?>
