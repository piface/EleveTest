<?php

class Student extends Model
{
    public $hasMany = [
        'Mark' => [
            'className' => 'Mark',
            'foreignKey' => 'student_id',
            'dependent' => 'true'
        ]
    ];

    public $validate = [
        'firstname' => [
            'rule1' => [
                'rule' => 'notBlank',
                'message' => 'Le prénom est obligatoire'
            ],
            'rule2' => [
                'rule' => '/^[a-zA-Z ]+$/',
                'message' => 'Le prenom doit uniquement contenir des lettres'
            ],
        ],
        'lastname' => [
            'rule1' => [
                'rule' => 'notBlank',
                'message' => 'Le nom est obligatoire'
            ],
            'rule2' => [
                'rule' => '/^[a-zA-Z ]+$/',
                'message' => 'Le nom doit uniquement contenir des lettres'
            ],
        ],
        'birthday' => [
            'rule1' => [
                'rule' => 'notBlank',
                'message' => 'La date est obligatoire'
            ],
            'rule2' => [
                'rule' => 'validDate',
                'message' => "La date de naissance ne doit peut pas être supérieur à la date d'aujourd'hui"
            ],
        ]
    ];

    public function validDate($createdAt)
    {
        return $createdAt['birthday'] < date('Y-m-d');
    }
}
