<?php

class Mark extends Model
{
    public $belongsTo = [
        'Student' => [
            'className' => 'Student',
            'foreignKey' => 'student_id'
        ],
        'Subject' => [
            'className' => 'Subject',
            'foreignKey' => 'subject_id',
            'dependent' => true
        ]
    ];

    public $validate = [
        'subject_id' => [
            'rule' => 'notBlank'
        ],
        'value' => [
            'rule1' => [
                'rule' => 'notBlank',
                'message' => 'La note est obligatoire',
            ],
            'rule2' => [
                'rule' => ['validMark'],
                'message' => 'La note doit être comprise entre 0 et 20',
            ],
        ]
    ];

    public function validMark($check)
    {
        return $check['value'] <= 20 && $check['value'] >= 0;
    }
}
